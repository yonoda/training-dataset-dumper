#ifndef SINGLE_BTAG_ALG_E_HH
#define SINGLE_BTAG_ALG_E_HH

#include "H5FileSvc.h"

#include "xAODBTagging/BTaggingContainer.h"
#include "xAODJet/JetContainer.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"
#include "GaudiKernel/ServiceHandle.h"

#include <memory>

class BTagElectronAugmenter; 
class AsgElectronSelectorTool; 
namespace H5 {
  class H5File;
}

class SingleBTagElectronAlg: public AthAlgorithm
{
public:
  SingleBTagElectronAlg(const std::string& name, ISvcLocator* pSvcLocator);
  ~SingleBTagElectronAlg();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
private:
  std::unique_ptr<BTagElectronAugmenter> m_electron_augmenter;
  std::unique_ptr<AsgElectronSelectorTool> m_electron_select;
};

#endif // SINGLE_BTAG_ALG_E_HH
