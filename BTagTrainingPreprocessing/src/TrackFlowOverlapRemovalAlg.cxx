/************************************************************
 * @file TrackFlowOverlapRemovalAlg.cxx
 * @brief Make a collection of tracks that aren't in the flow elements
 * **********************************************************/

#include "TrackFlowOverlapRemovalAlg.h"
#include "xAODJet/JetContainer.h"
#include "xAODPFlow/FlowElement.h"
#include "StoreGate/WriteDecorHandle.h"
#include "StoreGate/ReadDecorHandle.h"


TrackFlowOverlapRemovalAlg::TrackFlowOverlapRemovalAlg(
  const std::string& name, ISvcLocator* loc )
  : AthReentrantAlgorithm(name, loc) {}

StatusCode TrackFlowOverlapRemovalAlg::initialize() {
  ATH_MSG_DEBUG( "Inizializing " << name() << "... " );

  ATH_CHECK( m_tracksKey.initialize() );
  ATH_CHECK( m_constituentKey.initialize() );
  ATH_CHECK( m_tracksOutKey.initialize() );

  return StatusCode::SUCCESS;
}

StatusCode TrackFlowOverlapRemovalAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG( "Executing " << name() << "... " );

  using IPLV = std::vector<ElementLink<xAOD::IParticleContainer>>;

  SG::ReadDecorHandle<IPC, IPLV> tracks(m_tracksKey, ctx);
  SG::ReadDecorHandle<IPC, IPLV> constituents(m_constituentKey, ctx);
  SG::WriteDecorHandle<IPC, IPLV> tracksOut(m_tracksOutKey, ctx);

  for (const auto* obj: *constituents) {
    std::set<const xAOD::IParticle*> constituent_tracks;
    for (const auto& link: constituents(*obj)) {
      if (!link.isValid()) throw std::runtime_error(
        "invalid constituent link");
      const auto* flow = dynamic_cast<const xAOD::FlowElement*>(*link);
      if (!flow) throw std::runtime_error("constituent isn't flow object");
      if (!flow->isCharged()) continue;
      constituent_tracks.insert(flow->chargedObject(0));
    }
    IPLV non_constituent_tracks;
    for (const auto& link: tracks(*obj)) {
      if (!link.isValid()) throw std::runtime_error(
        "invalid track link");
      const xAOD::IParticle* track = *link;
      if (!constituent_tracks.count(track)) {
        non_constituent_tracks.push_back(link);
      }
    }
    tracksOut(*obj) = non_constituent_tracks;
  }

  return StatusCode::SUCCESS;
}
